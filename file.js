// 1. npm modules
var hbr = require('express3-handlebars'); 
var bodyParser = require('body-parser'); 
var mongoose = require('mongoose');
// 2. node modules
var express = require("express");
// 3. express initiation
var app = express();
// 4. static resources
// credentials
var credentials = require('./credentials.js');
// 5. engines
// handlebars
var handlebars = hbr.create({
defaultLayout:'main',
// Specify helpers which are only registered on this instance. helpers: {
 
section: function(name, options) {
if (!this._sections) this._sections = {}; this._sections[name] = options.fn(this); return null;
} }
);
app.engine('handlebars', handlebars.engine); app.set('view engine', 'handlebars');

// mongo db
mongoose.Promise = global.Promise;
var promise = mongoose.connect(credentials.mongoSecret, {
useMongoClient: true,
});
var User = require('./models/user.js');
// 6. port configuration
app.set('port', process.env.PORT || 8080); app.set('ip', process.env.IP || '0.0.0.0');
// 7. functions
// none for this app
// 8. middleware
// body-parser
app.use(bodyParser.json()); app.use(bodyParser.urlencoded({ extended: true }));

// 9. routes
app.get('/', function(req, res){
    User.find({}, function(err, users) {
res.render('users', {users: users, layout:null}); 
    
    });
});

app.get('/user', function(req, res) {
console.log('id (from querystring): ' + req.query.userid); User.findOne({_id: req.query.userid}, function(err, user) {
res.render('user', {user:user, layout:null}); });
});

app.get('/adduser', function(req, res) { res.render('adduser');
});
                                                                           
app.post('/adduser', function(req, res) { console.log('adding new user'+req.body); var newUser = new User(); newUser.name.first = req.body.firstName; newUser.name.last = req.body.lastName; newUser.email = req.body.emailAddr; newUser.homestate = req.body.homestate; newUser.save();
res.redirect(303, '/'); });

app.get('/delete', function(req, res) { User.remove({_id:req.query.userid}, function(err) {
res.redirect(303, '/'); });
});

app.get('/edit', function(req, res) { User.findById(req.query.userid, function(err, user) {
res.render('edituser', {user:user, layout:null}); });
});

app.post('/edituser', function(req, res) { console.log('updaLng user'+req.query.userid); var newemail = req.body.emailAddr;
var newhomestate = req.body.homestate; User.findByIdAndUpdate(req.query.userid,
{email: newemail, homestate: newhomestate}, {new:true}, function(err, user) {
res.render('user', {user:user, layout:null}); });
});

// 11. server configuration
app.listen(app.get('port'), app.get('ip'), () => {
console.log("Server listening on port " + app.get('port')+' on '+app.get('ip'));
});